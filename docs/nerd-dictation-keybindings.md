# A way to begin and end dictation

Adding some keyboard shortcut to begin and end dictation, respectively, is
simple. I would also like some on-screen indication while dictation is active,
perhaps in the same way that i3 modes are shown in the i3bar.

Notifiers (even if I could make them persist until the `end` command is given)
draw other elements, and would be too obtrusive.

I would really prefer a highlighted text in the i3bar saying `DICT` or something
along those lines.
Question is, how do we add such text to the i3bar without using an i3 mode?

```
{
 "full_text": "E: 10.0.0.1 (1000 Mbit/s)",
 "short_text": "10.0.0.1",
 "color": "#00ff00",
 "background": "#1c1c1c",
 "border": "#ee0000",
 "border_top": 1,
 "border_right": 0,
 "border_bottom": 3,
 "border_left": 1,
 "min_width": 300,
 "align": "right",
 "urgent": false,
 "name": "ethernet",
 "instance": "eth0",
 "separator": true,
 "separator_block_width": 9,
 "markup": "none"
}
```

+ https://i3wm.org/docs/i3bar-protocol.html


## My hack

In `~/.config/i3/config`, added

```
bindsym $ms+1 exec "/home/taha/.local/git/nerd-dictation/venv/bin/python /home/taha/.local/git/nerd-dictation/nerd-dictation begin"
bindsym $ms+Shift+1 exec "/home/taha/.local/git/nerd-dictation/venv/bin/python /home/taha/.local/git/nerd-dictation/nerd-dictation end"
```

where `$ms` is `Mod4` (Windows key).
Also, in ` i3status.sh`, added an item to display the word "DICTATION" if dictation
is ongoing, like so

```
#!/bin/bash
i3status -c ~/.config/i3/i3status.conf | while :
do
	read line
   dictation=$(ps aux | grep "nerd-dictation begin" | wc -l)
   if (( $dictation > 2 )); then
      echo "DICTATING! | $line" || exit 1
   fi
done
```

This is an ugly hack. No text colour or text background colour, which is unfortunate.
Might need to consider more high-level approaches to the whole i3status or i3bar
to finally achieve that.

+ https://github.com/vincent-petithory/i3cat
+ https://github.com/i3/i3/tree/next/contrib
+ https://i3wm.org/docs/i3bar-protocol.html
+ https://github.com/38/i3monkit
+ https://github.com/rholder/i3status-title-on-bar
