# i3wm

Ansible role that installs the latest available stable version from
the i3wm repos.
This role also configures such tools and functions that are immediately adjacent
to the i3 desktop, such as a workspace switcher (for multi-monitor workstation),
and rofi and its plugins.

There is only one thing stopping this role from working smoothly - the i3wm project
insists on periodically changing the name/path to their signing keys, which
makes the URL we have set in `defaults` fails every now and then
(easily fixed by finding the new URL of the latest signing key and checksum file).

But I wish i3wm would stick with a canonical path to the current signing key,
it would make for one less reason for failing playbooks.
If you happen to know of a better solution, do let me know!


## i3 layouts

I'm not sure if I have written extensively on this subject before.
If so, please try to collect all your work in one single place.

The problem is how to preserve workspace and window layout across reboots.

+ https://github.com/klaxalk/i3-layout-manager
  This is the most promising I think, plus appears still actively maintained.
  But even so, README says it's limited to only the current workspace. So I'm
  not sure whether it can restore all workspaces (if not, it's not very useful).
+ https://github.com/JonnyHaystack/i3-resurrect
  A simple but flexible solution to saving and restoring i3 workspaces
  This could be more straight-forward to understand and to integrate with i3 config
  and rofi. Has lots of examples, e.g.,
  https://github.com/JonnyHaystack/i3-resurrect/blob/master/contrib/scripts/i3-resurrect-dmenu
+ https://github.com/talwrii/i3-clever-layout
+ https://reddit.com/r/i3wm/comments/808685/create_your_own_i3_layouts_that_are_actually
+ https://unix.stackexchange.com/questions/134980/how-do-you-implement-layout-with-the-autostarting-of-applications-in-the-i3-wind


## i3 extensions and such

+ https://github.com/cornerman/i3-easyfocus
  This might be quite useful. A keyboard-only way to move focus between
  different windows on the workspace, even with complicated layouts.
+ https://github.com/budlabs/i3ass
  A whole suite of extensions, some intriguing components are:
  i3menu (extends rofi for i3wm) and i3zen (aka zen-mode).

## urxvt terminal themes (Xresources)

> When provisioning new workstations, or after changing `~/.Xresources`,
> remember that you **need to call `xrdb`'** to see the changes take effect:
> `xrdb ~/.Xresources`

My attempts to read the `DISPLAY` environment variable from the remote target all failed.
For example, `ansible.builtin.command: echo $DISPLAY` does not work, and
`ansible.builtin.shell: echo $DISPLAY` returns empty string.

Usually, preceding the command with `export DISPLAY=:0.0` would work, but I want this
command to work for virtual workstations, and they don't use `0` apparently.
For example, `wuhan` uses `DISPLAY=:50`.

+ https://stackoverflow.com/questions/27532173/how-to-put-the-result-of-an-echo-command-into-an-ansible-variable

Using ansible_env works, but naturally the env variable "DISPLAY" is not among those seen by Ansible:
```
# "i3wm_env": {
#   "changed": false,
#   "failed": false,
#   "msg": {
#     "DBUS_SESSION_BUS_ADDRESS": "unix:path=/run/user/1000/bus",
#     "HOME": "/home/taha",
#     "LANG": "en_US.UTF-8",
#     "LC_TIME": "sv_SE.UTF-8",
#     "LOGNAME": "taha",
#     "MAIL": "/var/mail/taha",
#     "PATH": "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin",
#     "PWD": "/home/taha",
#     "SHELL": "/bin/bash",
#     "SHLVL": "1",
#     "SSH_CLIENT": "192.168.1.101 56262 22",
#     "SSH_CONNECTION": "192.168.1.101 56262 192.168.1.111 22",
#     "SSH_TTY": "/dev/pts/2",
#     "TERM": "rxvt-unicode-256color",
#     "USER": "taha",
#     "XDG_RUNTIME_DIR": "/run/user/1000",
#     "XDG_SESSION_ID": "501",
#     "_": "/bin/sh"
#   }
# }
- name: Get environment from the target node
  debug:
    msg: "{{ ansible_env }}"
```

In light of the above, I have shelved my attempts to automate the call to `xrdb`.


+ https://asyncial.github.io/blog/organize-your-.xresources-like-a-pro/
+ https://bastian.rieck.me/blog/posts/2013/making_urxvt_beautiful/
+ https://addy-dclxvi.github.io/post/configuring-urxvt/
+ https://bcacciaaudio.com/2018/11/02/how-to-change-urxvt-terminal-emulator-colors/
+ https://github.com/AntSunrise/URxvt-themes
+ https://github.com/dracula/xresources
+ https://gitlab.com/trobador/urxvt-theme
+ https://reddit.com/r/i3wm/comments/2yytvs/make_terminals_transparent/
+ https://bbs.archlinux.org/viewtopic.php?id=51818&p=1
+ https://wiki.archlinux.org/index.php/Xterm#Colors
+ https://linux.die.net/man/1/xrdb
+ https://linux.die.net/man/7/x


## rofi user scripts

I would like some way to open a manuscript directory in the file browser
from rofi. Should be much quicker via rofi since I could just type out
the name "P03" and so on.

So I looked around, and realised that there is a small cottage industry of
[rofi user scripts](https://github.com/davatorium/rofi/wiki/User-scripts) built
around rofi's [scriptability](https://github.com/davatorium/rofi/blob/next/doc/rofi-script.5.markdown).

I found two file "openers":

+ https://github.com/marvinkreis/rofi-file-browser-extended
+ https://gitlab.com/matclab/rofi-file-selector

which one is more suitable?

Other scripts that look quite useful:

rofi-code. This looks nice. Should be easy to try out.
https://github.com/Coffelius/rofi-code

rofi-calc. This also looks very useful.
https://github.com/svenstaro/rofi-calc
But needs libqalculate *and* qalculate, which needs to be built from source, as far as I can tell.
https://github.com/svenstaro/rofi-calc/wiki/Installing-libqalculate-from-source

Bolt? Tries to do everything? I'm not sure it suits me.
https://github.com/salman-abedin/bolt

+ https://rahatzamancse.me/en/posts/you-can-make-anything-with-rofi/
+ https://adamsimpson.net/writing/getting-started-with-rofi

## passwordstore extensions

There is a small [ecosystem](https://github.com/tijn/awesome-password-store) of extensions
around [`pass`](https://www.passwordstore.org/) other than `rofi-pass`
that I might want to consider.

+ [Browserpass](https://github.com/browserpass/browserpass-native) and [its extension](https://github.com/browserpass/browserpass-extension)
+ [Pass OTP](https://github.com/tadfisher/pass-otp)
+ and others...


## i3wm tips and tricks

+ To get the current mapping of your keys, use `xmodmap -pke`.
+ To interactively enter a key and see what keysym it is configured to, use `xev`.
+ To display properties for an X window, use `xprop` or `xwininfo`.


## Refs

+ https://github.com/digimokan/ans_role_config_i3wm_wm (Ansible role, installs i3 from package manager)
+ https://lottalinuxlinks.com/adding-a-repurposed-caps-lock-as-a-third-mod-key-in-i3/
+ https://thevaluable.dev/i3-config-mouseless/
