---

# I have forked from upstream: https://gitlab.com/matclab/rofi-file-selector

- ansible.builtin.set_fact:
    rofi_file_selector_path: "{{ localgit_path }}/rofi-file-selector"
    rofi_remote_repo: "{{ gitea_user }}@{{ gitea_host }}:config/rofi-file-selector.git"
  tags: desktop-entry

### requirements
# + rofi >= 1.6.0 (we have v1.7.3)
# + bash >= 4.3, need to declare indirection variables (we have v5.1.16)
# + python >= 3.7 (v3.10)
# + PyGObject
# + fd, the fast find
# + choose, human-friendly and fast alternative to cut
# + xsel in order to copy the path to the clipboard with ctrl+c


# Does it really need python >= v3.7. Doesn't look like anything is affected by using 3.6.9.
# After some unintended testing, it looks like most things work even with 3.6.9.
# Except nothing is listed for any directories on my remote filesystem BAY.

### PyGObject
# https://pygobject.readthedocs.io/en/latest/getting_started.html#ubuntu-logo-ubuntu-debian-logo-debian
# installed in this venv using pip


### A human-friendly and fast alternative to cut and (sometimes) awk
# https://github.com/theryangeary/choose
- name: Install cargo
  ansible.builtin.apt:
    name: cargo
    state: present

- name: Install choose (the fast alternative to cut)
  ansible.builtin.command: cargo install choose

# cargo installs its binaries to /home/taha/.cargo/bin
# should we add the path to $PATH, or symlink each binary to ~/.local/bin/ ?
# lots of stuff in local/bin already, perhaps better to add it to PATH.
# OK added to PATH (see dotfiles role)

### A simple, fast and user-friendly alternative to 'find'
# https://github.com/sharkdp/fd
# For Ubuntu 18.04 and older, you can download the latest .DEB package from the release page
# https://github.com/sharkdp/fd/releases
# https://github.com/sharkdp/fd/releases/download/v8.2.1/fd-musl_8.2.1_amd64.deb
- name: Query Github API for the latest release of fd
  run_once: true
  delegate_to: localhost
  ansible.builtin.shell: >
    curl -s https://api.github.com/repos/sharkdp/fd/releases/latest |
    jq -r ".tag_name"
  register: fd_release_version

- name: Query Github API for the URL of fd's latest amd64 DEB
  run_once: true
  delegate_to: localhost
  ansible.builtin.shell: >
    curl -s https://api.github.com/repos/sharkdp/fd/releases/latest |
    jq -r ".assets[] | select(.name | test(\"amd64.deb$\")) | .browser_download_url" |
    tail -1
  register: fd_deb_release

- name: Check if we already have downloaded this version
  ansible.builtin.stat:
    path: "/var/cache/apt/archives/fd-{{ fd_release_version.stdout }}.deb"
  register: fd_deb_local


- name: Download and install
  when: not (fd_deb_local.stat.exists | bool)
  block:

    - name: "Download fd {{ fd_release_version.stdout }}"
      ansible.builtin.get_url:
        url: "{{ fd_deb_release.stdout }}"
        dest: "/var/cache/apt/archives/fd-{{ fd_release_version.stdout }}.deb"

    - name: "Install fd {{ fd_release_version.stdout }}"
      ansible.builtin.apt:
        deb: "/var/cache/apt/archives/fd-{{ fd_release_version.stdout }}.deb"
  # END OF BLOCK


### xsel
- name: Install xsel
  ansible.builtin.apt:
    name: xsel
    state: present

### clone rofi-file-selector repo
- name: Clone rofi-file-selector into ~/.local/git/
  ansible.builtin.git:
    repo: "{{ rofi_remote_repo}}"
    dest: "{{ rofi_file_selector_path }}"
    accept_hostkey: yes
  become: true
  become_user: "{{ ansible_env.USER }}"

- name: Install apt dependencies required by pip packages
  ansible.builtin.apt:
    name:
      - libgirepository1.0-dev
      - gcc
      - libcairo2-dev
      - pkg-config
      - gir1.2-gtk-3.0
    state: present

# create python venv for rofi-file-selector (latest available python version)
# This task fails on Ubuntu 22.04 with Python 3.10 with the error:
# "Unable to find pip in the virtualenv, /home/taha/.local/git/rofi-file-selector/venv, under any of these names: pip3, pip."
# Wait a minute, why are the binaries installed in venv/local/bin instead of venv/bin?
# https://askubuntu.com/questions/1406304/virtualenv-installs-envs-into-local-bin-instead-of-bin
# https://github.com/pypa/virtualenv/issues/2319
# So the problem lies with virtualenv itself, made obvious by recent changes in underlying Debian (posix_local scheme etc.)
# The temporary fix until this is fixed in virtualenv itself is probably to use a different
# virtualenv command (venv? pyvenv?), but for reasons unclear
# - name: "Install pip packages for rofi-file-selector in its own venv"
#   ansible.builtin.pip:
#     name:
#       - pycairo
#       - pygobject
#     state: present
#     virtualenv: "{{ rofi_file_selector_path }}/venv"
#     # "virtualenv" (default) causes task to fail, see notes above
#     # "pyvenv" fails with "Failed to find required executable 'pyvenv' in paths"
#     # "venv" fails with the same error
#     virtualenv_command: virtualenv
#     virtualenv_python: "python{{ python_version | trim }}"
#     # it's necessary to explicitly give base prefix (otherwise python assumes /usr and fails)
#     # https://github.com/pypa/pip/issues/9435
#     # https://stackoverflow.com/questions/25333640/pip-python-differences-between-install-option-prefix-and-root-and
#     extra_args: "--prefix={{ rofi_file_selector_path }}"
#   become: true
#   become_user: "{{ ansible_env.USER }}"

# only way to work-around the broken virtualenv (which in turn broke ansible.builtin.pip)
- name: "Create Python{{ python_version }} venv for rofi-file-selector"
  ansible.builtin.command: >
    /usr/bin/python{{ python_version }} -m venv
    {{ rofi_file_selector_path }}/venv
  become: true
  become_user: "{{ ansible_env.USER }}"

- name: "Install pip packages for rofi-file-selector in its own venv"
  ansible.builtin.command: >
    {{ rofi_file_selector_path }}/venv/bin/python -m pip install
    pycairo
    pygobject
  become: true
  become_user: "{{ ansible_env.USER }}"

# NOTE: configuration is handled in my repo itself, not in this ansible role

# instead of adding bindsym to i3 config, I chose to create a desktop entry
# that way, I don't need to remember a special keycombo, and can just
# open rofi-file-selector via the rofi menu
- name: Configure rofi-file-selector desktop launcher
  tags: desktop-entry
  become: true
  become_user: "{{ ansible_env.USER }}"
  block:

    - name: Ensure {{ xdg_desktop_entries }} directory exists
      ansible.builtin.file:
        path: "{{ xdg_desktop_entries }}"
        state: directory
        owner: "{{ ansible_env.USER }}"
        group: "{{ ansible_env.USER }}"

    - name: "Create desktop entry for rofi file selector"
      ansible.builtin.template:
        src: "rofi-file-selector.desktop.j2"
        dest: "{{ xdg_desktop_entries }}/rofi-file-selector.desktop"
        owner: "{{ ansible_env.USER }}"
        group: "{{ ansible_env.USER }}"
        mode: ug=rw,o=r
      register: desktop_entry
      notify: Update desktop entry database

    - name: "Validate desktop entry file"
      ansible.builtin.command: >
        /usr/bin/desktop-file-validate {{ desktop_entry.dest }}
  # END OF BLOCK
